from flask import Flask, g, jsonify
from io import BytesIO
from PIL import Image
import base64
import qrcode
import random
import sqlite3
import uuid
import hashlib

app = Flask(__name__)

DATABASE = 'messages.db'

# Initialize the database connection
def init_db():
    try:
        db = sqlite3.connect(DATABASE)
        db.execute('CREATE TABLE IF NOT EXISTS messages (id INTEGER PRIMARY KEY, message TEXT)')
        db.commit()
        db.close()
    except sqlite3.Error as e:
        print(f"Database error: {e}")

# Create a new database connection
def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(DATABASE)
    return g.db

# Initialize the database connection when the application starts up
@app.before_first_request
def before_first_request():
    init_db()

@app.route('/qr')
@app.route('/qr')
def generate_qr_code():
    try:
        # Retrieve a random message from the messages table in the messages.db database
        with get_db() as conn:
            cursor = conn.execute("SELECT message FROM messages ORDER BY RANDOM() LIMIT 1")
            message = cursor.fetchone()[0]

        # Generate a QR code with the message
        qr = qrcode.QRCode(version=1, box_size=10, border=5)
        qr.add_data(message)
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color="white")

        # Convert the image to a byte array and encode it using Base64
        buffered = BytesIO()
        img.save(buffered, format="PNG")
        img_str = base64.b64encode(buffered.getvalue()).decode("utf-8")

        # Return the QR code image as an HTML img tag with the message as the alt text
        return f'<img src="data:image/png;base64,{img_str}" alt="{message}">'

    except sqlite3.Error as e:
        return f"Database error: {e}", 500

    except Exception as e:
        return f"Error: {e}", 500

def decode_qr(qr_img_path):
    qr = cv2.imread(qr_img_path)
    qr_gray = cv2.cvtColor(qr, cv2.COLOR_BGR2GRAY)
    qr_coded = cv2.QRCodeDetector().detect(qr_gray)
    if len(qr_coded)>0:
        decoded_text, points, _ = cv2.QRCodeDetector().decode(qr_gray, qr_coded[0])
        uuid = decoded_text
        print("Scanned UUID:", uuid)
        message = messages.find_one({"uuid": uuid}) # retrieve message from database using UUID
        if message:
            print("Message:", message['message'])
        else:
            print("UUID not found in database.")
    else:
        print("QR code not detected.")

# Close the database connection when the application shuts down
@app.teardown_appcontext
def close_connection(exception):
    db = g.pop('db', None)

    if db is not None:
        db.close()

if __name__ == '__main__':
    app.run()
